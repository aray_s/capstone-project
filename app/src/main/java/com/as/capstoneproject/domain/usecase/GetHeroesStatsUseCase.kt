package com.`as`.capstoneproject.domain.usecase

import com.`as`.capstoneproject.domain.model.Hero
import com.`as`.capstoneproject.domain.repository.HeroesRepository
import javax.inject.Inject


class GetHeroesStatsUseCase @Inject constructor(
    private val heroesRepository: HeroesRepository
) {
    suspend operator fun invoke() = heroesRepository.getHeroesStats()
}