package com.`as`.capstoneproject.domain.repository

import com.`as`.capstoneproject.domain.model.Hero

interface HeroesRepository {
    suspend fun getHeroesStats(): List<Hero>

    suspend fun getFavoriteHeroesStats(): List<Hero>

    suspend fun addToFavorite(id: Int)

    suspend fun removeFromFavorite(id: Int)

    suspend fun getHero(id: Int): Hero
}