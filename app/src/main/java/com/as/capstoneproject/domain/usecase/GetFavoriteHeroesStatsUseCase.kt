package com.`as`.capstoneproject.domain.usecase

import com.`as`.capstoneproject.domain.repository.HeroesRepository
import javax.inject.Inject

class GetFavoriteHeroesStatsUseCase @Inject constructor (
    private val heroesRepository: HeroesRepository
) {
    suspend operator fun invoke() = heroesRepository.getFavoriteHeroesStats()
}