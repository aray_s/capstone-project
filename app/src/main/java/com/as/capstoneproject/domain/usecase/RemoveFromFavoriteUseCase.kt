package com.`as`.capstoneproject.domain.usecase

import com.`as`.capstoneproject.domain.repository.HeroesRepository
import javax.inject.Inject

class RemoveFromFavoriteUseCase @Inject constructor(private val heroesRepository: HeroesRepository
) {
    suspend operator fun invoke(id: Int) = heroesRepository.removeFromFavorite(id)
}