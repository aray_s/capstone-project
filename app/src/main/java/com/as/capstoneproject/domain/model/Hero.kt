package com.`as`.capstoneproject.domain.model

data class Hero(
    val id: Int,
    val name: String,
    val icon: String,
    val image: String,
    val attribute: String,
    val attack: String,
    val base_health: Int,
    val base_str: Int,
    val base_agi: Int,
    val base_int: Int,
    val str_gain: Float,
    val agi_gain: Float,
    val int_gain: Float,
    val isFavorite: Boolean
)