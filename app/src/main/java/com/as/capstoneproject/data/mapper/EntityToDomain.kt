package com.`as`.capstoneproject.data.mapper

import com.`as`.capstoneproject.data.model.HeroEntity
import com.`as`.capstoneproject.domain.model.Hero

class EntityToDomain {
    fun mapper(heroEntity: HeroEntity) = Hero(
        id=heroEntity.id,
        name=heroEntity.name,
        icon=heroEntity.icon,
        image=heroEntity.image,
        attribute=heroEntity.attribute,
        attack=heroEntity.attack,
        base_health=heroEntity.base_health,
        base_str=heroEntity.base_str,
        base_agi=heroEntity.base_agi,
        base_int=heroEntity.base_int,
        str_gain=heroEntity.str_gain,
        agi_gain=heroEntity.agi_gain,
        int_gain=heroEntity.int_gain,
        isFavorite = heroEntity.isFavorite==1
    )
}