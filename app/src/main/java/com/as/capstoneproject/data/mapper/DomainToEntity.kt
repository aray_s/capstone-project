package com.`as`.capstoneproject.data.mapper

import com.`as`.capstoneproject.data.model.HeroEntity
import com.`as`.capstoneproject.domain.model.Hero

class DomainToEntity {
    fun mapper(hero: Hero) = HeroEntity(
        id=hero.id,
        name=hero.name,
        icon=hero.icon,
        image=hero.image,
        attribute=hero.attribute,
        attack=hero.attack,
        base_health=hero.base_health,
        base_str=hero.base_str,
        base_agi=hero.base_agi,
        base_int=hero.base_int,
        str_gain=hero.str_gain,
        agi_gain=hero.agi_gain,
        int_gain=hero.int_gain,
        isFavorite = 0
    )
}