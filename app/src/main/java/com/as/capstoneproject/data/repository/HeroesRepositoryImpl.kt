package com.`as`.capstoneproject.data.repository

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import com.`as`.capstoneproject.data.storage.DataStorage
import com.`as`.capstoneproject.domain.model.Hero
import com.`as`.capstoneproject.domain.repository.HeroesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class HeroesRepositoryImpl(private val dataStorage: DataStorage, private val context: Context) :
    HeroesRepository {
    override suspend fun getHeroesStats(): List<Hero> {
        val activeNetworkInfo =
            (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo

        if (activeNetworkInfo != null && activeNetworkInfo.isConnected)
            dataStorage.saveHeroesStatstoDatabase()

        return dataStorage.getHeroesStatsfromDatabase()
    }

    override suspend fun getFavoriteHeroesStats(): List<Hero> =
        dataStorage.getFavoriteHeroesStatsfromDatabase()

    override suspend fun addToFavorite(id: Int) = dataStorage.addToFavoritetoDatabase(id)

    override suspend fun removeFromFavorite(id: Int) = dataStorage.removeFromFavoritetoDatabase(id)

    override suspend fun getHero(id: Int) = dataStorage.getHero(id)

}
