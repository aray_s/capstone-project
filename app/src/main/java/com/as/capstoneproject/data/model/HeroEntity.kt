package com.`as`.capstoneproject.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "heroes_table")
data class HeroEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "icon")
    val icon: String,
    @ColumnInfo(name = "image")
    val image: String,
    @ColumnInfo(name = "attribute")
    val attribute: String,
    @ColumnInfo(name = "attack")
    val attack: String,
    @ColumnInfo(name = "base_health")
    val base_health: Int,
    @ColumnInfo(name = "base_str")
    val base_str: Int,
    @ColumnInfo(name = "base_agi")
    val base_agi: Int,
    @ColumnInfo(name = "base_int")
    val base_int: Int,
    @ColumnInfo(name = "str_gain")
    val str_gain: Float,
    @ColumnInfo(name = "agi_gain")
    val agi_gain: Float,
    @ColumnInfo(name = "int_gain")
    val int_gain: Float,
    @ColumnInfo(name = "isFavorite")
    val isFavorite: Int
)
