package com.`as`.capstoneproject.data.storage

import com.`as`.capstoneproject.data.db.HeroDao
import com.`as`.capstoneproject.data.mapper.DomainToEntity
import com.`as`.capstoneproject.data.mapper.DtoToDomain
import com.`as`.capstoneproject.data.mapper.EntityToDomain
import com.`as`.capstoneproject.data.network.RetrofitServices
import com.`as`.capstoneproject.domain.model.Hero


class DataStorageImpl(
    private val heroDao: HeroDao, private val retrofitServices: RetrofitServices,
    private val mapperDomainToEntity: DomainToEntity, private val mapperDtoToDomain: DtoToDomain,
    private val mapperEntityToDomain: EntityToDomain
) : DataStorage {
    override suspend fun getHeroesStatsfromApi(): List<Hero> =
        retrofitServices.getHeroesStats().map(mapperDtoToDomain::mapper)

    override suspend fun getHeroesStatsfromDatabase(): List<Hero> =
        heroDao.getAll().map(mapperEntityToDomain::mapper)

    override suspend fun saveHeroesStatstoDatabase() = getHeroesStatsfromApi().forEach {
        if (!heroDao.isIdExist(it.id)) heroDao.insert(mapperDomainToEntity.mapper(it))
    }

    override suspend fun getFavoriteHeroesStatsfromDatabase(): List<Hero> =
        heroDao.getFavorities().map(mapperEntityToDomain::mapper)

    override suspend fun addToFavoritetoDatabase(id: Int) = heroDao.addToFavorite(id)
    override suspend fun removeFromFavoritetoDatabase(id: Int) = heroDao.removeFromFavorite(id)
    override suspend fun getHero(id: Int): Hero =
        mapperEntityToDomain.mapper(heroDao.getHerobyId(id))
}