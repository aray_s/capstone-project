package com.`as`.capstoneproject.data.network

import com.`as`.capstoneproject.data.model.HeroDto
import retrofit2.http.*

interface RetrofitServices {
    @GET("heroStats")
    suspend fun getHeroesStats(): List<HeroDto>
}

