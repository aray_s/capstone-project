package com.`as`.capstoneproject.data.mapper

import com.`as`.capstoneproject.data.model.HeroDto
import com.`as`.capstoneproject.domain.model.Hero

class DtoToDomain {
    fun mapper(heroDto: HeroDto) = Hero(
        id = heroDto.id,
        name = heroDto.name,
        icon = heroDto.icon,
        image = heroDto.image,
        attribute = heroDto.attribute,
        attack = heroDto.attack,
        base_health = heroDto.base_health,
        base_str = heroDto.base_str,
        base_agi = heroDto.base_agi,
        base_int = heroDto.base_int,
        str_gain = heroDto.str_gain,
        agi_gain = heroDto.agi_gain,
        int_gain = heroDto.int_gain,
        isFavorite = false
    )
}