package com.`as`.capstoneproject.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.`as`.capstoneproject.data.model.HeroEntity

@Dao
interface HeroDao {
    @Insert
    fun insert(hero: HeroEntity)

    @Update
    fun update(hero: HeroEntity)

    @Delete
    fun delete(hero: HeroEntity)

    @Query("UPDATE heroes_table SET isFavorite = 1 WHERE id =:id")
    fun addToFavorite(id: Int)

    @Query("UPDATE heroes_table SET isFavorite = 0 WHERE id =:id")
    fun removeFromFavorite(id: Int)

    @Query("SELECT * FROM heroes_table")
    fun getAll(): List<HeroEntity>

    @Query("SELECT * FROM heroes_table WHERE isFavorite = 1")
    fun getFavorities(): List<HeroEntity>

    @Query("SELECT EXISTS(SELECT * FROM heroes_table WHERE id = :id)")
    fun isIdExist(id : Int) : Boolean

    @Query("SELECT * FROM heroes_table WHERE id=:id")
    fun getHerobyId(id: Int) : HeroEntity
}
