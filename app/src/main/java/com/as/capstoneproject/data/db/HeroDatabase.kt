package com.`as`.capstoneproject.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.`as`.capstoneproject.data.model.HeroEntity

@Database(entities = arrayOf(HeroEntity::class), version = 1, exportSchema = false)
abstract class HeroDatabase : RoomDatabase() {
    abstract fun heroDao(): HeroDao

    companion object {
        @Volatile
        private var INSTANCE: HeroDatabase? = null
        fun getDatabase(context: Context): HeroDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    HeroDatabase::class.java,
                    "heroes_database"
                ) .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
