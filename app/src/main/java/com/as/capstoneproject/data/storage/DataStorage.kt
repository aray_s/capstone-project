package com.`as`.capstoneproject.data.storage

import com.`as`.capstoneproject.domain.model.Hero

interface DataStorage {
    suspend fun getHeroesStatsfromApi(): List<Hero>

    suspend fun getHeroesStatsfromDatabase(): List<Hero>

    suspend fun getFavoriteHeroesStatsfromDatabase(): List<Hero>

    suspend fun saveHeroesStatstoDatabase()

    suspend fun addToFavoritetoDatabase(id: Int)

    suspend fun removeFromFavoritetoDatabase(id: Int)

    suspend fun getHero(id: Int): Hero
}
