package com.`as`.capstoneproject.data.model

import com.google.gson.annotations.SerializedName

data class HeroDto(
    val id: Int,
    @SerializedName("localized_name") val name: String,
    val icon: String,
    @SerializedName("img") val image: String,
    @SerializedName("primary_attr") val attribute: String,
    @SerializedName("attack_type") val attack: String,
    val base_health: Int,
    val base_str: Int,
    val base_agi: Int,
    val base_int: Int,
    val str_gain: Float,
    val agi_gain: Float,
    val int_gain: Float
)