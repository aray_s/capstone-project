package com.`as`.capstoneproject.di

import com.`as`.capstoneproject.domain.repository.HeroesRepository
import com.`as`.capstoneproject.domain.usecase.AddToFavoriteUseCase
import com.`as`.capstoneproject.domain.usecase.GetFavoriteHeroesStatsUseCase
import com.`as`.capstoneproject.domain.usecase.GetHeroByIdUseCase
import com.`as`.capstoneproject.domain.usecase.GetHeroesStatsUseCase
import com.`as`.capstoneproject.domain.usecase.RemoveFromFavoriteUseCase
import com.`as`.capstoneproject.presentation.ui.details.DetailsViewModelFactory
import com.`as`.capstoneproject.presentation.ui.favorite.FavoriteViewModelFactory
import com.`as`.capstoneproject.presentation.ui.home.HomeViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class PresentationModule {

    @Provides
    fun provideHomeViewModelFactory(getHeroesStatsUseCase: GetHeroesStatsUseCase): HomeViewModelFactory =
        HomeViewModelFactory(getHeroesStatsUseCase)

    @Provides
    fun provideDetailsViewModelFactory(getHeroByIdUseCase: GetHeroByIdUseCase, addToFavoriteUseCase: AddToFavoriteUseCase, removeFromFavoriteUseCase: RemoveFromFavoriteUseCase): DetailsViewModelFactory =
        DetailsViewModelFactory(getHeroByIdUseCase, addToFavoriteUseCase, removeFromFavoriteUseCase)

    @Provides
    fun provideFavoriteViewModelFactory(getFavoriteHeroesStatsUseCase: GetFavoriteHeroesStatsUseCase): FavoriteViewModelFactory =
        FavoriteViewModelFactory(getFavoriteHeroesStatsUseCase)
    @Provides
    fun provideGetHeroByIdUseCase(heroesRepository: HeroesRepository) = GetHeroByIdUseCase(heroesRepository)

    @Provides
    fun provideAddToFavoriteUseCase(heroesRepository: HeroesRepository) = AddToFavoriteUseCase(heroesRepository)

    @Provides
    fun provideGetFavoriteHeroesStatsUseCase(heroesRepository: HeroesRepository) = GetFavoriteHeroesStatsUseCase(heroesRepository)

    @Provides
    fun provideRemoveFromFavoriteUseCase(heroesRepository: HeroesRepository) = RemoveFromFavoriteUseCase(heroesRepository)

    @Provides
    fun provideGetHeroesStatsUseCase(heroesRepository: HeroesRepository) = GetHeroesStatsUseCase(heroesRepository)
}