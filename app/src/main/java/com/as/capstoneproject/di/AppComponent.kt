package com.`as`.capstoneproject.di

import com.`as`.capstoneproject.presentation.ui.details.DetailsFragment
import com.`as`.capstoneproject.presentation.ui.favorite.FavoriteFragment
import com.`as`.capstoneproject.presentation.ui.home.HomeFragment
import dagger.Component

@Component(modules = [AppModule::class, PresentationModule::class])
interface AppComponent {
    fun inject(fragment: HomeFragment)

    fun inject(fragment: DetailsFragment)

    fun inject(fragment: FavoriteFragment)
}
