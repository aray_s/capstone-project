package com.`as`.capstoneproject.di

import android.content.Context
import androidx.room.Room
import com.`as`.capstoneproject.data.db.HeroDao
import com.`as`.capstoneproject.data.db.HeroDatabase
import com.`as`.capstoneproject.data.mapper.DomainToEntity
import com.`as`.capstoneproject.data.mapper.DtoToDomain
import com.`as`.capstoneproject.data.mapper.EntityToDomain
import com.`as`.capstoneproject.data.network.RetrofitClient
import com.`as`.capstoneproject.data.network.RetrofitServices
import com.`as`.capstoneproject.data.repository.HeroesRepositoryImpl
import com.`as`.capstoneproject.data.storage.DataStorage
import com.`as`.capstoneproject.data.storage.DataStorageImpl
import com.`as`.capstoneproject.domain.repository.HeroesRepository
import dagger.Module
import dagger.Provides

@Module
class AppModule(val context: Context) {

    @Provides
    fun provideContext(): Context = context

    @Provides
    fun provideRetrofitServices(): RetrofitServices = RetrofitClient.retrofitServices

    @Provides
    fun provideHeroDatabase(context: Context): HeroDatabase =
        Room.databaseBuilder(context, HeroDatabase::class.java, "HeroesDatabase").build()

    @Provides
    fun provideHeroDao(heroDatabase: HeroDatabase): HeroDao = heroDatabase.heroDao()

    @Provides
    fun provideDataStorage(
        heroDao: HeroDao,
        retrofitServices: RetrofitServices,
        mapperDomainToEntity: DomainToEntity,
        mapperDtoToDomain: DtoToDomain,
        mapperEntityToDomain: EntityToDomain,
    ): DataStorage = DataStorageImpl(
        heroDao,
        retrofitServices,
        mapperDomainToEntity,
        mapperDtoToDomain,
        mapperEntityToDomain,
    )

    @Provides
    fun provideHeroesRepository(dataStorage: DataStorage, context: Context): HeroesRepository =
        HeroesRepositoryImpl(dataStorage, context)

    @Provides
    fun provideDtoToEntity(): DtoToDomain = DtoToDomain()

    @Provides
    fun provideDomainToEntity(): DomainToEntity = DomainToEntity()

    @Provides
    fun provideEntityToDomain(): EntityToDomain = EntityToDomain()

}

