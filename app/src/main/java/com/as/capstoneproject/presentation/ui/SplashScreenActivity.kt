package com.`as`.capstoneproject.presentation.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.`as`.capstoneproject.R

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
        if (prefs.getBoolean(THEME, true)) AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        else AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        setContentView(R.layout.activity_splash_screen)
        if (prefs.getBoolean(SHOW_SPLASH,true)){

            Handler().postDelayed({ //This method will be executed once the timer is over
                // Start your app main activity
                val i = Intent(this@SplashScreenActivity, MainActivity::class.java)
                startActivity(i)
                // close this activity
                finish()
            }, 2000)
        }
        else {
            val i = Intent(this@SplashScreenActivity, MainActivity::class.java)
            startActivity(i)
            finish()
        }
    }

    private companion object{
        const val SETTINGS = "SETTINGS"
        const val THEME = "THEME"
        const val SHOW_SPLASH="SHOW_SPLASH"
    }
}