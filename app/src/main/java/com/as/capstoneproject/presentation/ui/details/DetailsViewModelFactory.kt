package com.`as`.capstoneproject.presentation.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.`as`.capstoneproject.domain.usecase.AddToFavoriteUseCase
import com.`as`.capstoneproject.domain.usecase.GetHeroByIdUseCase
import com.`as`.capstoneproject.domain.usecase.RemoveFromFavoriteUseCase

class DetailsViewModelFactory(private val getHeroByIdUseCase: GetHeroByIdUseCase, private val addToFavoriteUseCase: AddToFavoriteUseCase, private val removeFromFavoriteUseCase: RemoveFromFavoriteUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return DetailsViewModel(getHeroByIdUseCase, addToFavoriteUseCase, removeFromFavoriteUseCase) as T
    }
}