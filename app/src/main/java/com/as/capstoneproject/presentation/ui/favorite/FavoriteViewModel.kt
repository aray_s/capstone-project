package com.`as`.capstoneproject.presentation.ui.favorite

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.`as`.capstoneproject.domain.model.Hero
import com.`as`.capstoneproject.domain.usecase.GetFavoriteHeroesStatsUseCase
import com.`as`.capstoneproject.domain.usecase.RemoveFromFavoriteUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FavoriteViewModel(private val getFavoriteHeroesStatsUseCase: GetFavoriteHeroesStatsUseCase) : ViewModel() {

    val favoriteHeroesStats = MutableLiveData<List<Hero>>()

    fun getFavoriteHeroesStats() = viewModelScope.launch(Dispatchers.IO) {
        favoriteHeroesStats.postValue(getFavoriteHeroesStatsUseCase.invoke())
    }
}