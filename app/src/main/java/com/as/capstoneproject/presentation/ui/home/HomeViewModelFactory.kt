package com.`as`.capstoneproject.presentation.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.`as`.capstoneproject.domain.usecase.GetHeroesStatsUseCase

class HomeViewModelFactory(private val getHeroesStatsUseCase: GetHeroesStatsUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return HomeViewModel(getHeroesStatsUseCase) as T
    }
}