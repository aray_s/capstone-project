package com.`as`.capstoneproject.presentation.ui.favorite

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.`as`.capstoneproject.R
import com.`as`.capstoneproject.databinding.FragmentFavoriteBinding
import com.`as`.capstoneproject.di.App
import com.`as`.capstoneproject.presentation.adapter.HeroesAdapter
import javax.inject.Inject


class FavoriteFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: FavoriteViewModelFactory

    private lateinit var binding: FragmentFavoriteBinding
    private lateinit var viewModel: FavoriteViewModel
    private lateinit var adapter: HeroesAdapter
    private lateinit var navController: NavController

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as App).appComponent.inject(this)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFavoriteBinding.inflate(inflater, container, false)
        navController = findNavController()
        viewModel = ViewModelProvider(this, viewModelFactory)[FavoriteViewModel::class.java]

        adapter = HeroesAdapter(requireContext(), onClick = {
            val action = FavoriteFragmentDirections.actionToDetailsFragment(it.id)
            navController.navigate(action)
        }
        )

        binding.recyclerViewFavorite.adapter = adapter
        binding.recyclerViewFavorite.layoutManager = LinearLayoutManager(requireContext())

        viewModel.favoriteHeroesStats.observe(viewLifecycleOwner) { list -> list?.let { adapter.updateList(it) } }
        viewModel.getFavoriteHeroesStats()

        return binding.root
    }
}