package com.`as`.capstoneproject.presentation.ui

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import com.`as`.capstoneproject.R
import com.`as`.capstoneproject.databinding.ActivitySettingsBinding


class SettingsActivity: AppCompatActivity() {

    private lateinit var binding: ActivitySettingsBinding
    private lateinit var prefs: SharedPreferences

    private var show: Boolean = true
    private var theme: Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        prefs = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
        setContentView(binding.root)

        binding.showSplashScreen.setOnCheckedChangeListener { _, id ->
            show = when (id) {
                R.id.show -> true
                R.id.dontShow -> false
                else -> throw IllegalArgumentException("Unknown status")
            }
        }

        binding.themeGroup.setOnCheckedChangeListener { _, id ->
            theme = when (id) {
                R.id.turnOn -> {
                    //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                    true
                }
                R.id.turnOff -> {
                    //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                    false
                }
                else -> throw IllegalArgumentException("Unknown theme")
            }
        }

        binding.saveButton.setOnClickListener {
            if (theme) AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            else AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

            prefs.edit().run {
                putBoolean(SHOW_SPLASH, show)
                putBoolean(THEME, theme)
                apply()
            }
        }
    }

    private companion object{
        const val SETTINGS = "SETTINGS"
        const val THEME = "THEME"
        const val SHOW_SPLASH="SHOW_SPLASH"
    }
}