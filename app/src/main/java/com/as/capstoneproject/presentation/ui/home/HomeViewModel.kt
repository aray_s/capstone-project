package com.`as`.capstoneproject.presentation.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.`as`.capstoneproject.domain.model.Hero
import com.`as`.capstoneproject.domain.usecase.GetHeroesStatsUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(private val getHeroesStatsUseCase: GetHeroesStatsUseCase) : ViewModel() {
    val heroStats = MutableLiveData<List<Hero>>()

    fun getHeroesStats() = viewModelScope.launch(Dispatchers.IO) {
        heroStats.postValue(getHeroesStatsUseCase.invoke())
    }
}

