package com.`as`.capstoneproject.presentation.ui.details

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.`as`.capstoneproject.R
import com.`as`.capstoneproject.databinding.FragmentDetailsBinding
import com.`as`.capstoneproject.di.App
import com.bumptech.glide.Glide
import javax.inject.Inject


class DetailsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: DetailsViewModelFactory

    private lateinit var binding: FragmentDetailsBinding
    private lateinit var navController: NavController
    private lateinit var viewModel: DetailsViewModel
    private val args: DetailsFragmentArgs by navArgs()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as App).appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this, viewModelFactory)[DetailsViewModel::class.java]
        navController = findNavController()
        viewModel.setHero(args.id)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)

        viewModel.hero.observe(viewLifecycleOwner) { hero ->

            binding.tvHeroName.text = hero.name

            Glide.with(binding.ivHeroImage)
                .load("https://api.opendota.com${hero.image}")
                .fitCenter()
                .into(binding.ivHeroImage)

            viewModel.isFavorite=hero.isFavorite

            if (viewModel.isFavorite) {
                binding.ivIsSaved.setImageResource(R.drawable.baseline_favorite_24)
            } else  binding.ivIsSaved.setImageResource(R.drawable.baseline_favorite_border_24)

            binding.ivIsSaved.setOnClickListener {
                if (viewModel.isFavorite) {
                    binding.ivIsSaved.setImageResource(R.drawable.baseline_favorite_border_24)
                    viewModel.removeFromFavorite(args.id)
                }
                else {
                    binding.ivIsSaved.setImageResource(R.drawable.baseline_favorite_24)
                    viewModel.addToFavorite(args.id)
                }
                viewModel.isFavorite=!viewModel.isFavorite
            }

            binding.layoutType.apply{
                tvHeroAttribute.text  = getHeroAttributeName(hero.attribute)
                tvHeroAttack.text = hero.attack
                ivHeroAttribute.setImageResource(getHeroAttributeImage(hero.attribute))
                ivHeroAttack.setImageResource(getHeroAttackImage(hero.attack))
            }

            binding.layoutCharacteristics.apply {
                tvAgilityStats.text = getString(R.string.stats, hero.base_agi, hero.agi_gain)
                tvIntelligenceStats.text = getString(R.string.stats, hero.base_int, hero.int_gain)
                tvStrengthStats.text = getString(R.string.stats, hero.base_str, hero.str_gain)
                tvHealthStats.text = hero.base_health.toString()
            }
        }

//        activity?.findViewById<Toolbar>(R.id.toolbar)?.setNavigationOnClickListener {
//            findNavController().navigateUp()
//        }

        return binding.root
    }

    private fun getHeroAttackImage(attackType: String): Int {
        return when (attackType) {
            "Melee" -> R.drawable.ic_melee
            "Ranged" -> R.drawable.ic_ranged
            else -> R.drawable.ic_universal
        }
    }

    private fun getHeroAttributeImage(attribute: String): Int {
        return when (attribute) {
            "agi" -> R.drawable.ic_agility
            "str" -> R.drawable.ic_strength
            "int" -> R.drawable.ic_intelligence
            else -> R.drawable.ic_universal
        }
    }

    private fun getHeroAttributeName(attribute: String): String {
        return when (attribute) {
            "agi" -> getString(R.string.agility)
            "str" -> getString(R.string.strength)
            "int" -> getString(R.string.intelligence)
            else -> getString(R.string.universal)
        }
    }
}