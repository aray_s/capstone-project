package com.`as`.capstoneproject.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.`as`.capstoneproject.R
import com.`as`.capstoneproject.databinding.ItemBinding
import com.`as`.capstoneproject.domain.model.Hero
import com.bumptech.glide.Glide

class HeroesAdapter (private val context: Context, private val onClick: (Hero)->Unit) : RecyclerView.Adapter<HeroesAdapter.ViewHolder>(){

    private lateinit var binding: ItemBinding
    private var heroesList = mutableListOf<Hero>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        binding = ItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hero = heroesList[position]
        holder.name.text = hero.name
        Glide.with(holder.icon)
            .load("https://api.opendota.com${hero.icon}")
            .fitCenter()
            .into(holder.icon)
        holder.attr.text = getHeroAttributeName(hero.attribute)
        holder.attrImage.setImageResource(getHeroAttributeImage(hero.attribute))


        holder.itemView.setOnClickListener { onClick(hero) }
    }

    fun updateList(newList: List<Hero>) {
        heroesList.clear()
        heroesList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun getItemCount() = heroesList.size

    inner class ViewHolder(binding: ItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val name = binding.tvHeroName
        val icon = binding.ivHeroImage
        val attr = binding.tvHeroAttribute
        val attrImage = binding.ivHeroAttribute
    }

    fun getHeroAttributeImage(attribute: String): Int {
        return when (attribute) {
            "agi" -> R.drawable.ic_agility
            "str" -> R.drawable.ic_strength
            "int" -> R.drawable.ic_intelligence
            else -> R.drawable.ic_universal
        }
    }

    fun getHeroAttributeName(attribute: String): String {
        return when (attribute) {
            "agi" -> "agility"
            "str" -> "strength"
            "int" -> "intelligence"
            else -> "universal"
        }
    }
}

