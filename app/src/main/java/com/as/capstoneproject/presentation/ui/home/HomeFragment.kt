package com.`as`.capstoneproject.presentation.ui.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.`as`.capstoneproject.R
import com.`as`.capstoneproject.databinding.FragmentHomeBinding
import com.`as`.capstoneproject.di.App
import com.`as`.capstoneproject.presentation.adapter.HeroesAdapter
import javax.inject.Inject


class HomeFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: HomeViewModelFactory

    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var adapter: HeroesAdapter
    private lateinit var navController: NavController

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as App).appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentHomeBinding.inflate(inflater, container, false)
        navController = findNavController()
        viewModel = ViewModelProvider(this, viewModelFactory)[HomeViewModel::class.java]

        adapter = HeroesAdapter(requireContext(), onClick = {
            val action = HomeFragmentDirections.actionToDetailsFragment(it.id)
            navController.navigate(action)
        }
        )

        binding.recyclerViewHome.adapter = adapter
        binding.recyclerViewHome.layoutManager = LinearLayoutManager(requireContext())

        viewModel.heroStats.observe(viewLifecycleOwner) { list -> list?.let { adapter.updateList(it) } }
        viewModel.getHeroesStats()

        return binding.root
    }

}