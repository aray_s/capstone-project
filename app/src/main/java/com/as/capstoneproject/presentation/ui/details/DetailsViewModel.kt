package com.`as`.capstoneproject.presentation.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.`as`.capstoneproject.domain.model.Hero
import com.`as`.capstoneproject.domain.usecase.AddToFavoriteUseCase
import com.`as`.capstoneproject.domain.usecase.GetHeroByIdUseCase
import com.`as`.capstoneproject.domain.usecase.RemoveFromFavoriteUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailsViewModel(private val getHeroByIdUseCase: GetHeroByIdUseCase, private val addToFavoriteUseCase: AddToFavoriteUseCase, private val removeFromFavoriteUseCase: RemoveFromFavoriteUseCase) : ViewModel() {

    var hero = MutableLiveData<Hero>()
    var isFavorite : Boolean = false

    fun setHero(id:Int) = viewModelScope.launch(Dispatchers.IO) {
        hero.postValue(getHeroByIdUseCase.invoke(id))
    }

    fun addToFavorite(id:Int) = viewModelScope.launch(Dispatchers.IO) {
        addToFavoriteUseCase.invoke(id)
    }

    fun removeFromFavorite(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        removeFromFavoriteUseCase.invoke(id)
    }
}