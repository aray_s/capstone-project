package com.`as`.capstoneproject.presentation.ui.favorite

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.`as`.capstoneproject.domain.usecase.GetFavoriteHeroesStatsUseCase

class FavoriteViewModelFactory(private val getFavoriteHeroesStatsUseCase: GetFavoriteHeroesStatsUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return FavoriteViewModel(getFavoriteHeroesStatsUseCase) as T
    }
}