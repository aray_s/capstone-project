package com.`as`.capstoneproject

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withParent
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.`as`.capstoneproject.presentation.ui.MainActivity
import org.hamcrest.core.AllOf.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ExampleInstrumentedTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    private lateinit var scenario: ActivityScenario<MainActivity>

    @Before
    fun setUp() {
        scenario = activityRule.scenario
    }

    @Test
    fun performOnCLickSettings() {
        onView(withId(R.id.settingsButton))
            .perform(click())

        onView(withId(R.id.activity_settings_layout))
            .check(matches(isDisplayed()))
    }

    @Test
    fun performOnCLickHome() {
        onView(withId(R.id.navigation_home))
            .perform(click())

        onView(withId(R.id.fragment_home_layout))
            .check(matches(isDisplayed()))
    }

    @Test
    fun performOnCLickFavorite() {
        onView(withId(R.id.navigation_favorite))
            .perform(click())

        onView(withId(R.id.fragment_favorite_layout))
            .check(matches(isDisplayed()))
    }

    @Test
    fun performOnCLickRecyclerViewItemInHome() {
        onView(withId(R.id.navigation_home))
            .perform(click())

        onView(allOf(withId(R.id.recyclerViewHome), withParent(withId(R.id.fragment_home_layout))))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))

        onView(allOf(withId(R.id.ivHeroImage), isDescendantOfA(withId(R.id.fragment_details_layout))))
    }

    @Test
    fun performOnCLickRecyclerViewItemInFavorite() {
        onView(withId(R.id.navigation_favorite))
            .perform(click())

        onView(allOf(withId(R.id.recyclerViewFavorite), withParent(withId(R.id.fragment_favorite_layout))))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))

        onView(allOf(withId(R.id.ivHeroImage), withParent(withId(R.id.fragment_details_layout))))
            .check(matches(isDisplayed()))
    }
}
