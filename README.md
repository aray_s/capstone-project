Capstone Project for EPAM Android Developer Internship

This is an app that lists all Dota's heroes list.

- CLean Architecture, MVVM
- Kotlin, Coroutines
- Room
- Dagger2
- Navigation Component
- Retrofit, used API https://api.opendota.com/api/
- Glide
- Espresso

![image](/uploads/117d24bab8e3a2b0981b8b908a89b014/image.png){width=200}

![image](/uploads/42e46a3769a9ca7b3a449b0cc4f1e111/image.png){width=200}

![image](/uploads/c7386b91de326e15ffe11aecf709c87c/image.png){width=200}

![image](/uploads/a0a4f64f95faa0f16dda9ddde1353648/image.png){width=200}
